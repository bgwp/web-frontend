# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Vue - Official](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (previously Volar) and disable Vetur

## 环境准备

- Node: v20.13.1

## 安装依赖

```bash
npm install
```

## 本地运行
1. 启动前端服务
```bash
npm run dev
```
## 配置代理
### 为什么需要配置代理
因为QQ登录回调的域名是写死的，且使用cookie作为凭证，因此本地`localhost`并没有办法携带`bbkgames.com`域名下的cookie。
因此这里就需要使用`https://www.bbkgames.com/`访问我们的本地服务

### 如何配置
我本地采用的方案是`whistle + SwitchyOmega`的方式，当然要实现上面的方式也不止这一种
#### 安装并配置whistle
可以参考该文档: https://wproxy.org/whistle/install.html
1. 安装
```bash
npm i whistle -g
```

2. 启动代理
```bash
w2 start
```

3. 安装ssl证书：
这里参考：https://wproxy.org/whistle/webui/https.html

4. 配置rules：
```
https://www.bbkgames.com http://localhost:8888
https://www.bbkgames.com/ http://localhost:8888
```

#### 安装并配置SwitchyOmega
这里我用chrome举例，edge和chrome类似，推荐使用这两种浏览器，其他浏览器可以自行查找安装方法。

1. 安装
  - 打开Chrome浏览器
  - 进入`管理扩展程序`
  - 进入`扩展商店`
  - 搜索`SwitchyOmega`并安装
2. 配置：
参考这个文档里面的`SwitchyOmega`的设置：https://wproxy.org/whistle/install

#### 完成进度
##### 快捷游戏板块 —— 游戏快捷接口
完成进度: 90%
+ 存在的问题：
  - 目前固定推送前三个，但是看老网站的图片貌似不是前三个，这个是hard code还是遵循某个规则推荐？
  - 目前游戏界面还是老界面

##### 玩家账号板块 —— 玩家账号登录/玩家信息显示/云存档信息显示/玩家详细信息二级页面
完成进度: 90%
+ 存在的问题：
  - 目前接口只能拿到**用户头像 / 用户昵称 / 用户最近的存档**，具体需要显示哪些信息可否列出来？以及哪些是现有的，哪些是需要再开发的。
##### 开发群组板块 —— 开发群组联系方式/公众号平台/其他平台等
完成进度：90%
+ 存在的问题：
  - 目前在老网站能拿到的只有：**QQ群名称/QQ群号/QQ群二维码/微信公众号名称/微信公众号/微信公众号二维码**。其他平台具体指哪些平台，需要列出来。
##### 资讯信息板块 —— 游戏资讯更新/攻略分享/公众号推文链接（同人创作区）/交流区/BUG反馈区
完成进度: 0%
##### 游戏接口板块 —— 新版/旧版游戏接口
完成进度: 0%
