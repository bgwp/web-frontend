import { authRequest, bayeRequest } from "./request"

const NOW_TIMESTAMP = Date.now();

export const getUserInfo = () => authRequest.get('/user_info')

export const getSaveIndex = saveDir => bayeRequest.get(`/save/${saveDir}/index.json?t=${NOW_TIMESTAMP}`)

export const getAllGamsLibs = () => bayeRequest.get(`/libs.json?ver=${NOW_TIMESTAMP}`)
