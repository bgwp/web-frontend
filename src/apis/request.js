import axios from "axios"

export const authRequest = axios.create({
  baseURL: import.meta.env.VITE_AUTH_API_BASE
})

export const bayeRequest = axios.create({
  baseURL: import.meta.env.VITE_BAYE_API_BASE
})
