import { defineStore } from "pinia";
import { ref } from "vue";
import { getAllGamsLibs, getSaveIndex } from "../apis";

export const useGamesStore = defineStore('games', () => {
  const gamesList = ref([]);
  const fetchAllGames = async () => {
    const { data } = await getAllGamsLibs();
    gamesList.value = data;
  }
  return { games: gamesList, fetchAllGames };
})

export const useCloudSavesStore = defineStore('cloudSaves', () => {
  const cloudSaves = ref([]);
  const fetchUserCloudSaves = async (saveDir) => {
    const { data } = await getSaveIndex(saveDir);
    cloudSaves.value = data;
  }

  return { cloudSaves, fetchUserCloudSaves };
});
