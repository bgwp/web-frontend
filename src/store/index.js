import { useUserStore } from "./userInfo";
import { useGamesStore, useCloudSavesStore } from "./games";
import { watch } from "vue";
import { defineStore } from "pinia";

export const useAppStore = defineStore('appStore', () => {
  const userStore = useUserStore()
  const gamesStore = useGamesStore();
  const cloudSavesStore = useCloudSavesStore();
  function installAppStore(app) {
    app.config.globalProperties.$userStore = userStore;
    app.config.globalProperties.$gamesStore = gamesStore;
    app.config.globalProperties.$cloudSavesStore = cloudSavesStore;

    userStore.getUserInfo();
    gamesStore.fetchAllGames();
    watch(
      () => userStore.userInfo,
      (userInfo, preUserInfo) => {
        if (!userInfo) return;
        if (userInfo.sav_dir === preUserInfo?.sav_dir) return;
        cloudSavesStore.fetchUserCloudSaves(userInfo.sav_dir);
      },
    )
  }
  return {
    userStore,
    gamesStore,
    cloudSavesStore,
    installAppStore,
  }
}) 
