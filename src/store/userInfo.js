import { defineStore } from "pinia"
import { getUserInfo as _getUserInfo } from "../apis"
import { ref } from "vue"

export const useUserStore = defineStore('userInfo', () => {
  const userInfo = ref(null);
  const getUserInfo = async () => {
    const res = await _getUserInfo()
    if(res.data && res.data.avatar) {
      userInfo.value = res.data 
    }
  }
  return { userInfo, getUserInfo }
})
