const clearLib = window.clearLib;

function redirect(page) {
  const uiKind = window.localStorage['baye/uiKind'];
  let isMobile;

  switch (uiKind) {
      case "mobile":
          isMobile = true;
          break;
      case "desktop":
          isMobile = false;
          break;
      default:
          isMobile = "detect";
          break;
  }

  if (isMobile == "detect") {
      isMobile = navigator.userAgent.match(/(iPhone|iPod|Android|ios|Mobile|ARM)/i);
  }

  if(isMobile){
      let defaultMPage = "m.html";
      switch (window.localStorage['baye/mpage']) {
      case '0':
          defaultMPage = "m.html"
          break;
      case '1':
          defaultMPage = "m-old.html"
          break;
      case '2':
          defaultMPage = "m-ges.html"
          break;
      case '3':
          defaultMPage = "m-ktouch.html"
          break;
      }
      page = page || defaultMPage;
  } else {
      page = "pc.html";
  }
  const now = new Date().getTime() / 1000;
  const name = getLibName();
  const hash = isMobile ? "#" + now : "";
  const host = 'https://baye.bbkgames.com/'
  window.location.href = host + page + "?name=" + name + hash;
}

export function loadGame({ title, path }) {
  clearLib(() => {
		//http direct
		if (0 === path.indexOf('http')){
			location.href=path;
			return;
		}

    if (path && path.length > 0) {
        window.localStorage['baye/libname'] = title;
        window.localStorage['baye/libpath'] = path;
    }
    redirect();
  });
}
