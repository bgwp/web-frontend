import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import mkcert from'vite-plugin-mkcert'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    https: false,
    port: 8888,
    proxy: {
      '/oauth-api': {
        target: 'https://oauth.bbkgames.com/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/oauth-api/, ''),
      },
      '/baye-api': {
        target: 'https://baye.bbkgames.com/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/baye-api/, ''),
      }
    }
  },
  resolve: {
    alias: {
      /** @ 符号指向 src 目录 */
      "@": path.resolve(__dirname, "./src")
    }
  },
  plugins: [
    vue(), 
    mkcert(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
})
